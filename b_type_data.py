# type data ada 5 yaitu integer,float,string,boolean,complex

#INTEGER
#type data angka tanpa koma
print("=====Data Integer=====")
data_integer = 10
print("Data : ",data_integer)
print(f"Bertipe {type(data_integer)}")

#Float
#type data angka dengan koma
print("=====Data Float=====")
data_float = 10.99
print("Data : ",data_float)
print(f"Bertipe {type(data_float)}")

#String
#Type data karakter bisa berupa huruf/angka dan data biasanya di apit oleh (' atau ")
print("=====Data String=====")
data_string = "udin"
print("Data : ",data_string)
print(f"Bertipe {type(data_string)}")

data_string2 = "10"
print("Data : ",data_string2)
print(f"Bertipe {type(data_string2)}")

#Boolean
#type data berupa biner 0/1 (True/False)
print("=====Data Boolean=====")
data_bool = True
print("Data : ",data_bool)
print(f"Bertipe {type(data_bool)}")

data_bool2 = False
print("Data : ",data_bool2)
print(f"Bertipe {type(data_bool2)}")

#Complex
#type data gabungan dengan data ke-2 adalah imajiner
print("=====Data Complex=====")
data_Complex = complex(6,4)
print("Data : ",data_Complex)
print(f"Bertipe {type(data_Complex)}")