#Casting data

#Integer
print("=====Integer=====")
data_int = 10
print(f"data = {data_int}, type = {type(data_int)}")
print("=====Casting=====")

data_float  = float(data_int)
data_str    = str(data_int)
data_bool   = bool(data_int) #akan False jika nilai int 0

print(f"data float      = {data_float}, type = {type(data_float)}")
print(f"data string     = {data_str}, type = {type(data_str)}")
print(f"data boolean    = {data_bool}, type = {type(data_bool)}")

#Float
print("=====Float=====")
data_float = 10.99
print(f"data = {data_float}, type = {type(data_float)}")
print("=====Casting=====")

data_int  = int(data_float) # akan dibulatkan kebawah
data_str    = str(data_float)
data_bool   = bool(data_float) #akan False jika nilai int 0

print(f"data integer    = {data_int}, type = {type(data_int)}")
print(f"data string     = {data_str}, type = {type(data_str)}")
print(f"data boolean    = {data_bool}, type = {type(data_bool)}")

#Boolean
print("=====Boolean=====")
data_bool = False
print(f"data = {data_bool}, type = {type(data_bool)}")
print("=====Casting=====")

data_int    = int(data_bool) # 1 = true , 0 = false
data_str    = str(data_bool) # akan menjadi string
data_float  = float(data_bool) # akan 1.0 = true / akan 0.0 = False

print(f"data integer    = {data_int}, type = {type(data_int)}")
print(f"data string     = {data_str}, type = {type(data_str)}")
print(f"data float      = {data_float}, type = {type(data_float)}")

#String
print("=====String=====")
data_str = "10"
print(f"data = {data_str}, type = {type(data_str)}")
print("=====Casting=====")

data_int    = int(data_str) # harus angka
data_bool    = str(data_str) # kosong akan false
data_float  = float(data_str) # harus angka

print(f"data integer    = {data_int}, type = {type(data_int)}")
print(f"data Boolean    = {data_bool}, type = {type(data_bool)}")
print(f"data float      = {data_float}, type = {type(data_float)}")